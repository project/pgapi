<?php

// Constant definition
define('cPaypalActionUrl',     'https://www.paypal.com/xclick/');
define('cPaypalValidationUrl', 'https://www.paypal.com/cgi-bin/webscr');

/**
 * Implements hook_menu().
 */
function pg_paypal_menu() {
  $items = array();
  $items['paypal/done'] = array(
    'title' => 'Internal Data',
    'page callback' => 'pg_paypal_done_payment',
    'access callback' => TRUE,
    'file' => 'pg_paypal.pages.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pgdata/pgsettings/paypal'] = array(
    'title' => 'Paypal',
    'description' => 'Paypal payment settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pg_paypal_settings'),
    'access arguments' => array('administer pgapi'),
    'file' => 'pg_paypal.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_pgapi_gw().
 */
function pg_paypal_pgapi_gw($op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'display name':
      $ret = t('Paypal Payment Gateway');
      break;
    case 'payment gateway info':
      $ret['name'] = theme('image', array(
        'path' => drupal_get_path('module', 'pg_paypal') . 'images//paypal.png',
        'width' => t('Paypal'),
        'height' => t('Paypal'),
      ));
      $prices[variable_get('pg_paypal_currency_code', 'USD')] = round(($a3 * variable_get('pg_paypal_rate', '1.00')), 2);
      $ret['price'] = $prices;
      break;
    case 'get form':
      $transaction = $a3;
      $ret = drupal_get_form('pg_paypal_extraform', $transaction);
      break;
    case 'process form':
      $transaction = $a3;
      $ret = _pg_paypal_process_form($transaction);
      break;
  }
  return $ret;
}

/**
 * Implements hook_pgapi_transaction().
 */
function pg_paypal_pgapi_transaction($status, &$transaction) {
  switch ($status) {
    case PG_PENDING:
      $transaction->extra['amount'] =  round($transaction->amount * variable_get('pg_paypal_rate', '1.00'), 2);
      break;
  }
}

/**
 * Return paypal extra form
 */
function pg_paypal_extraform($form, &$form_state, $transaction) {
  $form['message'] = array(
    '#type' => 'markup',
    '#value' => '<!-- -->',
  );
  return $form;
}

/**
 * Process Paypal payment form
 */
function _pg_paypal_process_form($transaction) {
  $amount = number_format($transaction->amount * variable_get('pg_paypal_rate', '1.00'), 2);
  $transaction->extra['amount'] = $amount;
  $transaction->gateway = 'pg_paypal';
  $output = drupal_get_form('pg_paypal_payform', $transaction);
  pgapi_transaction_save($transaction);
  echo '<html><body>' . $output . '<script>pgpayform.submit();</script></body>';
  exit();
}

/**
 * Return paypal payment form
 */
function pg_paypal_payform($form, &$form_state,  $transaction) {
  global $user;
  global $language;

  $form['lc'] = array(
    '#type' => 'hidden',
    '#value' => $language->language,
  );
  $form['item_name'] = array(
    '#type' => 'hidden',
    '#value' => $transaction->title,
  );
  $form['charset'] = array(
    '#type' => 'hidden',
    '#value' => 'UTF-8',
  );
  $form['amount'] = array(
    '#type' => 'hidden',
    '#value' => $transaction->extra['amount'],
  );
  $form['business'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('pg_paypal_email', ''),
  );
  $form['item_number'] = array(
    '#type' => 'hidden',
    '#value' => $transaction->txnid,
  );
  $form['cbt'] = array(
    '#type' => 'hidden',
    '#value' => t('Return back to !site', array('!site' => variable_get('site_name', 'Drupal'))),
  );
  $form['currency_code'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('pg_paypal_currency_code', ''),
  );
  $form['notify_url'] = array(
    '#type' => 'hidden',
    '#value' => url('paypal/done', array('absolute' => TRUE)),
  );
  $form['return'] = array(
    '#type' => 'hidden',
    '#value' => url(pgapi_get_redirect_url($transaction), array('absolute' => TRUE)),
  );
  $form['cancel_return'] = array(
    '#type' => 'hidden',
    '#value' => url(pgapi_get_redirect_url($transaction), array('absolute' => TRUE)),
  );
  $form['#attributes'] = array('name' => 'pgpayform');
  $form['#action'] = variable_get('pg_paypal_action_url', cPaypalActionUrl);
  return $form;
}
