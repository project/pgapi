<?php

/*
 * @file
 * pg_roboxchange.admin.inc
 * Admin interface for ROBOX module
 */

/**
 * Settings form for roboxchange
 */
function pg_roboxchange_settings($form, &$form_state) {

  if (FALSE === @('XML/Unserializer.php')) {
    drupal_set_message(t('You have to install !url, otherwise RoboXchange Gateway will not work.', array('!url' => l('XML/Serializer', 'http://pear.php.net/package/XML_Serializer'))), 'error');
  }

  $form['#tree'] = TRUE;
  $form['pg_roboxchange_rate_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Rate url'),
    '#default_value' => variable_get('pg_roboxchange_rate_url', cRoboXchangeRateUrl),
    '#description' => t("Please enter rate url. Default: !url", array('!url' => cRoboXchangeRateUrl)),
  );

  if (!variable_get('pg_roboxchange_mrh', '')) {
    $form['rate'] = array(
      '#type' => 'fieldset',
      '#title' => t('Rate information'),
      '#description' => t('Please fill and save "Merchant Login" first'),
    );
  }
  else {
    pg_roboxchange_cron();

    $form['rate'] = array(
      '#type' => 'fieldset',
      '#title' => t('Rate information'),
      '#theme' => 'pg_roboxchange_settings_table',
      '#description' => t('Please check all currency that you want to accept'),
    );
    $form['rate']['currency'] = array(
      '#type' => 'markup',
      '#value' => variable_get('pg_roboxchange_out_curr', ''),
    );
    $form['rate']['last_update'] = array(
      '#type' => 'markup',
      '#value' => variable_get('pg_roboxchange_last_update', ''),
    );
    $result = db_query('SELECT * FROM {pg_roboxchange_rates} ORDER BY type');
    $checked = variable_get('pg_roboxchange_checked', '');
    foreach ($result as $rr) {
      $form['rate']['type'][$rr->type] = array(
        '#type' => 'checkbox',
        '#default_value' => $checked[$rr->type],
      );
      $form['rate']['name'][$rr->type] = array(
        '#type' => 'markup',
        '#value' => $rr->name,
      );
      $form['rate']['rate'][$rr->type] = array(
        '#type' => 'markup',
        '#value' => $rr->rate,
      );
    }
  }

  $form['pg_roboxchange_action_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Action url'),
    '#default_value' => variable_get('pg_roboxchange_action_url', cRoboXchangeActionUrl),
    '#description' => t("Please enter action url.<br/>Default: !url<br/>Test: !url_test", array('!url' => pg_roboxchange_server_url('live'), '!url_test' => pg_roboxchange_server_url('test'))),
  );
  $form['pg_roboxchange_mrh'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant Login'),
    '#default_value' => variable_get('pg_roboxchange_mrh', ''),
    '#description' => t("Please enter Merchant Login."),
    '#required' => TRUE,
  );
  $form['pg_roboxchange_mps1'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant Pass 1'),
    '#default_value' => variable_get('pg_roboxchange_mps1', ''),
    '#description' => t("Please enter Merchant Login."),
    '#required' => TRUE,
  );
  $form['pg_roboxchange_mps2'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant Pass 2'),
    '#default_value' => variable_get('pg_roboxchange_mps2', ''),
    '#description' => t("Please enter Merchant Login."),
    '#required' => TRUE,
  );
  $form['pg_roboxchange_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Rate to site currency'),
    '#default_value' => variable_get('pg_roboxchange_rate', '1.00'),
    '#description' => t("Please enter RoboXchange rate according to site currency."),
  );
  $form['url'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Urls(result,success,fail) for merchant interfase'),
  );
  $form['url']['result'] = array(
    '#type' => 'textfield',
    '#title' => t('Result url'),
    '#value' => url('roboxchange/done', array('absolute' => TRUE)),
    '#description' => t("Just for copy."),
    '#attributes' => array('readonly' => '1'),
  );
  $form['url']['success'] = array(
    '#type' => 'textfield',
    '#title' => t('Success url'),
    '#value' => url('roboxchange/success', array('absolute' => TRUE)),
    '#description' => t("Just for copy."),
    '#attributes' => array('readonly' => '1'),
  );
  $form['url']['fail'] = array(
    '#type' => 'textfield',
    '#title' => t('Fail url'),
    '#value' => url('roboxchange/fail', array('absolute' => TRUE)),
    '#description' => t("Just for copy."),
    '#attributes' => array('readonly' => '1'),
  );
  $form['#validate'][] = 'pg_roboxchange_settings_validate';
  $form['#submit'][] = 'pg_roboxchange_settings_submit';
  return system_settings_form($form);
}

/**
 * Validate function for roboxchange settings form
 */
function pg_roboxchange_settings_validate($form, &$form_state) {
  if ($form_state['values']['pg_roboxchange_rate'] <= 0) {
    form_set_error('pg_roboxchange_rate', t('%rate must be more 0.', array('%rate' => $form_state['values']['pg_roboxchange_rate'])));
  }
}

/**
 * Submit function for roboxchange settings form
 */
function pg_roboxchange_settings_submit($form, &$form_state) {
  if (isset($form_state['values']['pg_roboxchange_action_url'])) {
    variable_set('pg_roboxchange_action_url', $form_state['values']['pg_roboxchange_action_url']);
  }
  if (isset($form_state['values']['pg_roboxchange_rate_url'])) {
    variable_set('pg_roboxchange_rate_url', $form_state['values']['pg_roboxchange_rate_url']);
  }
  if (isset($form_state['values']['pg_roboxchange_mrh'])) {
    variable_set('pg_roboxchange_mrh', $form_state['values']['pg_roboxchange_mrh']);
  }
  if (isset($form_state['values']['pg_roboxchange_mps1'])) {
    variable_set('pg_roboxchange_mps1', $form_state['values']['pg_roboxchange_mps1']);
  }
  if (isset($form_state['values']['pg_roboxchange_mps2'])) {
    variable_set('pg_roboxchange_mps2', $form_state['values']['pg_roboxchange_mps2']);
  }
  if (isset($form_state['values']['pg_roboxchange_rate'])) {
    variable_set('pg_roboxchange_rate', $form_state['values']['pg_roboxchange_rate']);
  }
  if (isset($form_state['values']['rate']['type'])) {
    variable_set('pg_roboxchange_checked', $form_state['values']['rate']['type']);
  }
}
