<?php

/**
 * @file
 *   Theme functions for roboxchange module
 */

/**
 * Process settings form to make table from it
 */
function theme_pg_roboxchange_settings_table($variables) {
  $form = $variables['form'];
  $output  = '<strong' . t('Your output currency') . ':</strong>' . drupal_render($form['currency']);
  $output .= '<strong' . t('Last update time') . ':</strong>' . drupal_render($form['last_update']);

  // Create table
  $header = array('', t('Name'), t('Rate'));
  $rows = array();
  foreach (element_children($form['name']) as $key) {
    $row = array();
    if (is_array($form['name'][$key])) {
      $row[]  = drupal_render($form['type'][$key]);
      $row[]  = drupal_render($form['name'][$key]);
      $row[]  = drupal_render($form['rate'][$key]);
      $rows[] = $row;
    }
  }
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => 'data')));
  $output .= drupal_render_children($form);
  return $output;
}
